<?php

namespace BlogBundle\Controller;

use BlogBundle\Entity\Tag;
use BlogBundle\Entity\billets;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\View;

class TagController extends Controller
{
    /**
     * Lists all tag entities.
     *
     * @Rest\Get("tags/")
     * @view(serializerGroups = {"list"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tags = $em->getRepository('BlogBundle:Tag')->findAll();

        return $tags
        ;
    }

    /**
     * Creates a new tag entity.
     *
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("tags/")
     * @view(serializerGroups = {"list"})
     */
    public function newAction(Request $request)
    {
        $tag = new Tag();
        $form = $this->createForm('BlogBundle\Form\TagType', $tag);
        $tag->setName($request->get('name'));
        //$form->submit($request->request->all());

        $billet = $this->get('doctrine.orm.entity_manager')
                ->getRepository('BlogBundle:billets')
                ->find($request->get('idBillet'));

        $tag->addBillet($billet);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tag);
            $em->flush();

            return $tag
            ;
        }

        return $form
        ;
    }

    /**
     * Finds and displays a tag entity.
     *
     * @Rest\Get("tags/{id}")
     */
    public function showAction(Tag $tag)
    {
        $deleteForm = $this->createDeleteForm($tag);
        
        if (empty($tag)) {
                    return new JsonResponse(['message' => 'User not found'], Response::HTTP_NOT_FOUND);
        }

        return $tag
        ;
    }

    /**
     * Displays a form to edit an existing tag entity.
     *
     * @Route("tags/{id}/edit", name="tag_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Tag $tag)
    {
        $deleteForm = $this->createDeleteForm($tag);
        $editForm = $this->createForm('BlogBundle\Form\TagType', $tag);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('tag_edit', array('id' => $tag->getId()));
        }

        return $this->render('tag/edit.html.twig', array(
            'tag' => $tag,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a tag entity.
     *
     * @Route("tags/{id}", name="tag_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Tag $tag)
    {
        $form = $this->createDeleteForm($tag);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tag);
            $em->flush();
        }

        return $this->redirectToRoute('tag_index');
    }

    /**
     * Creates a form to delete a tag entity.
     *
     * @param Tag $tag The tag entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Tag $tag)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tag_delete', array('id' => $tag->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

       /**
     * Finds and displays a comment entity.
     *
     * @Rest\Get("billets/{id}/tags")
     */
     public function getCommentsByBilletAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $tags = $em->getRepository('BlogBundle:Tag')->getTagsByBillet($id);
        
        return $tags;
    }
}
