<?php

namespace BlogBundle\Controller;

use BlogBundle\Entity\Comments;
use BlogBundle\Entity\billets;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\View;
use Nelmio\ApiDocBundle\Annotation as Doc;

class commentsController extends Controller
{
    /**
     * @Doc\ApiDoc(
     *     resource=true,
     *     description="Get the list of all comments."
     * )
     *
     * Lists all comment entities.
     *
     * @Rest\Get("commentaires/")
     * @view(serializerGroups = {"list"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $comments = $em->getRepository('BlogBundle:Comments')->getAllPublish();
        
        return $comments;
    }

    /**
     * @Doc\ApiDoc(
     *     resource=true,
     *     description="Create a comment."
     * )
     *
     * Creates a new comment entity.
     *
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("commentaires/")
     */
    public function newAction(Request $request)
    {
        $comment = new Comments();
        $form = $this->createForm('BlogBundle\Form\commentsType', $comment);
        $form->submit($request->request->all());
        //$billet = new billets();
        $billet = $this->get('doctrine.orm.entity_manager')
                ->getRepository('BlogBundle:billets')
                ->find($request->get('idBillet'));

        $comment->setBillet($billet);
        $comment->setCreatedAt();
        $comment->setStatus("publish");
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();

            return $comment;

        } else {
            return $form;
        }

    }

    /**
     * @Doc\ApiDoc(
     *     resource=true,
     *     description="Get one comment."
     * )
     *
     * Finds and displays a comment entity.
     *
     * @Rest\Get("commentaires/{id}")
     */
    public function showAction(comments $comment)
    {
        $deleteForm = $this->createDeleteForm($comment);

        if (empty($comment)) {
            return new JsonResponse(['message' => 'comment not found'], Response::HTTP_NOT_FOUND);
        }

        return $comment;
    }

    /**
    * @Doc\ApiDoc(
     *     resource=true,
     *     description="Moderate one comment."
     * )
     *
     * Moderate a comment entity.
     *
     * @Rest\View()
     * @Rest\Put("commentaires/{id}")
     */
    public function moderateAction(Request $request, comments $comment)
    {
        $deleteForm = $this->createDeleteForm($comment);
        $editForm = $this->createForm('BlogBundle\Form\commentsType', $comment);

        $editForm->submit($request->request->all(), false);

        if ($editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $comment;
        }

        return $editForm;
    }

    /**
     * Displays a form to edit an existing comment entity.
     *
     * @Route("commentaires/{id}/edit", name="comments_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, comments $comment)
    {
        $deleteForm = $this->createDeleteForm($comment);
        $editForm = $this->createForm('BlogBundle\Form\commentsType', $comment);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('comments_edit', array('id' => $comment->getId()));
        }

        return $this->render('comments/edit.html.twig', array(
            'comment' => $comment,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Doc\ApiDoc(
     *     resource=true,
     *     description="Delete one comment."
     * )
     *
     * Deletes a comment entity.
     *
     * @Route("commentaires/{id}", name="comments_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, comments $comment)
    {
        $form = $this->createDeleteForm($comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($comment);
            $em->flush();
        }

        return $this->redirectToRoute('comments_index');
    }

    /**
     * Creates a form to delete a comment entity.
     *
     * @param comments $comment The comment entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(comments $comment)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('comments_delete', array('id' => $comment->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

     /**
     * @Doc\ApiDoc(
     *     resource=true,
     *     description="Get all comments of one billet."
     * )
     *
     * Finds and displays a comment entity.
     *
     * @Rest\Get("billets/{id}/commentaires")
     * @view(serializerGroups = {"list"})
     */
     public function getCommentsByBilletAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $comments = $em->getRepository('BlogBundle:Comments')->getCommentsByBillet($id);
        
        return $comments;
    }
}
