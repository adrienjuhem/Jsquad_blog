<?php

namespace BlogBundle\Controller;

use BlogBundle\Entity\billets;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\View;
use Nelmio\ApiDocBundle\Annotation as Doc;

class billetsController extends Controller
{
    /**
     * @Doc\ApiDoc(
     *     resource=true,
     *     description="Get the list of all billets."
     * )
     *
     * Lists all billet entities.
     * @Rest\Get("billets/")
     * @view(serializerGroups = {"list"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $billets = $em->getRepository('BlogBundle:billets')->getAllOrderByDate();


        foreach ($billets as $billet) {
            foreach ($billet->getComments() as $comment) {
                if($comment->getStatus() != 'publish') {
                    $billet->removeComment($comment);
                }
            }
        }
        return $billets;
    }

     /**
     * @Doc\ApiDoc(
     *     resource=true,
     *     description="Create a billet."
     * )
     *
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("billets/")
     */
     public function newAction(Request $request)
     {
        $billet = new billets();
        $form = $this->createForm('BlogBundle\Form\billetsType', $billet);
        $form->submit($request->request->all()); 
        $billet->setCreatedAt();

        if($billet->getImgUrl() === null) {
            $number = rand(1, 6);

            $billet->setImgUrl("http://backend.adrien-juhem.eu/img/img". $number .".jpg");
        }
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($billet);
            $em->flush();

            return $billet;
        } else {
            return $form;
        }

    }

    /**
     * @Doc\ApiDoc(
     *     resource=true,
     *     description="Get one billet."
     * )
     *
     * Finds and displays a billet entity.
     * @Rest\Get("billets/{id}")
     * @view(serializerGroups = {"list"})
     */
    public function showAction(billets $billet)
    {
        $deleteForm = $this->createDeleteForm($billet);

        if (empty($billet)) {
            return new JsonResponse(['message' => 'billet not found'], Response::HTTP_NOT_FOUND);
        }
        
        foreach ($billet->getComments() as $comment) {
            if($comment->getStatus() != 'publish') {
                $billet->removeComment($comment);

            }
        }


        return $billet;
    }

    /**
     * @Doc\ApiDoc(
     *     resource=true,
     *     description="Update one billet."
     * )
     *
     * Displays a form to edit an existing billet entity.
     *
     * @Route("billets/{id}", name="billets_edit")
     * @Method({"PUT"})
     */
    public function editAction(Request $request, billets $billet)
    {
        $deleteForm = $this->createDeleteForm($billet);
        $editForm = $this->createForm('BlogBundle\Form\billetsType', $billet);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('billets_edit', array('id' => $billet->getId()));
        }

        return $this->render('billets/edit.html.twig', array(
            'billet' => $billet,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            ));
    }

    /**
     * @Doc\ApiDoc(
     *     resource=true,
     *     description="Delete one billet."
     * )
     *
     * Deletes a billet entity.
     *
     * @Route("billets/{id}", name="billets_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, billets $billet)
    {
        $form = $this->createDeleteForm($billet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($billet);
            $em->flush();
        }

        return $this->redirectToRoute('billets_index');
    }

    /**
     * Creates a form to delete a billet entity.
     *
     * @param billets $billet The billet entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(billets $billet)
    {
        return $this->createFormBuilder()
        ->setAction($this->generateUrl('billets_delete', array('id' => $billet->getId())))
        ->setMethod('DELETE')
        ->getForm()
        ;
    }
}
