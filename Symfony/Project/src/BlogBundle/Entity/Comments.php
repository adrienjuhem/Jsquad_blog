<?php

namespace BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Comments
 *
 * @ORM\Table(name="comments")
 * @ORM\Entity(repositoryClass="BlogBundle\Repository\CommentsRepository")
 */
class Comments
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"list"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="string", length=255)
     * @Serializer\Groups({"list"})
     */
    private $content;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Serializer\Groups({"list"})
     */
    private $createdAt;

    /**
     * @var string
     *
     * @Serializer\Groups({"list"})
     * @ORM\Column(name="author", type="string", length=255)
     */
    private $author;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, options={"default":"publish"})
     */
    private $status;

    /**
     * @var int
     *
     * @ORM\Column(name="id_billet", type="integer")
     * @Serializer\Groups({"list"})
     */
    private $idBillet;

     /**
     * 
     * @ORM\ManyToOne(targetEntity="billets", inversedBy="comments", cascade={"persist"})
     * @ORM\JoinColumn(name="id_billet", referencedColumnName="id")
     */
     private $billet;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Comments
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Comments
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set author
     *
     * @param string $author
     *
     * @return Comments
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }


    /**
     * Set billet
     *
     * @param \BlogBundle\Entity\billet $billet
     *
     * @return Comments
     */
    public function setBillet(\BlogBundle\Entity\billets $billet = null)
    {
        $this->billet = $billet;

        return $this;
    }

    /**
     * Get billet
     *
     * @return \BlogBundle\Entity\billet
     */
    public function getBillet()
    {
        return $this->billet;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Comments
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Gets the value of idBillet.
     *
     * @return int
     */
    public function getIdBillet()
    {
        return $this->idBillet;
    }

    /**
     * Sets the value of idBillet.
     *
     * @param int $idBillet the id billet
     *
     * @return self
     */
    public function setIdBillet($idBillet)
    {
        $this->idBillet = $idBillet;

        return $this;
    }
}
