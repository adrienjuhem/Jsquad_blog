import { Component, OnInit }  from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { BlogPost }           from '../model/blogPost';
import { Comment }            from '../model/comment';
import { BlogPostService }    from '../service/blogPosts.service';
import { AppComment }         from '../comment/app.comment';

@Component({
  selector: 'board-detail',
  templateUrl: 'app/board-detail/app.board-detail.html',
  styleUrls: ['app/board-detail/app.board-detail.css'],
  providers: [BlogPostService]
})

export class AppBoardDetail {
  private id: number;
  private sub: any;
  private blogPosts : BlogPost;
  private blogPost : BlogPost[] = [];

  constructor(private route: ActivatedRoute, private blogPostService : BlogPostService) {}

  private getBlogPostOne(id : number){
    this.blogPostService.getBlogPostOne(id).subscribe(blogPosts => {
      this.blogPosts = blogPosts;
      this.blogPost.push(this.blogPosts);
      console.log(this.blogPost);
    });
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
       this.id = params['id'];
    });
    this.getBlogPostOne(this.id);
  }
}
