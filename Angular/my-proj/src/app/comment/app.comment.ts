import { Component, Input, OnInit } from '@angular/core';
import { BlogPostService }          from '../service/blogPosts.service';

import { Comment }                  from '../model/comment';

@Component({
  selector: 'comment',
  templateUrl: 'app/comment/app.comment.html',
  styleUrls: ['app/comment/app.comment.css'],

  providers: [BlogPostService],
})
export class AppComment {
  @Input() blogPostId:  string;
  comments: Comment[];
  displayFormComment =  false;
  author:               string;
  content:              string;
  idBillet:             string;

  constructor(private blogPostService: BlogPostService) {
  }

  private getComment() {
    this.blogPostService.getComment().subscribe(comments => {
      this.comments = comments;
      return comments;
    });
  }

  private moderate(id : number){
    this.blogPostService.updateCommenStatus("not appropriate", id).subscribe(comment => {
      this.comments = this.comments.filter(function(el) { // create a new array without comment with id to moderates
        return el.id != id;
      });
    });
  }

  private displayOrNot() {
    this.displayFormComment = !this.displayFormComment;
  }

  private save() {
    this.blogPostService.createComment(this.author, this.content, this.blogPostId).subscribe(comment => {
      this.comments.push(comment);
      this.author = null;
      this.content = null;
      this.displayFormComment = !this.displayFormComment;
      });
  }

  ngOnInit(): void {
    this.getComment();
    console.log(this.blogPostId);
  }
}
