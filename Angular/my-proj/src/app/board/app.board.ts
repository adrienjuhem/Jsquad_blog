import { Component, OnInit }  from '@angular/core';

import { BlogPost }           from '../model/blogPost';
import { Comment }            from '../model/comment';
import { BlogPostService }    from '../service/blogPosts.service';
import { AppComment }         from '../comment/app.comment';


@Component({
  selector: '',
  templateUrl: 'app/board/app.board.html',
  styleUrls: ['app/board/app.board.css'],
  providers: [BlogPostService]
})

export class AppBoard {

  blogPosts: BlogPost[];
  blogTags: any[];

  constructor(private blogPostService: BlogPostService) { }

  private getBlogPosts() {
    this.blogPostService.getBlogPosts().subscribe(blogPosts => {
      this.blogPosts = blogPosts;
    });
  }

  private getBlogTags() {
    this.blogPostService.getBlogTags().subscribe(blogTags => {
      this.blogTags = blogTags;
    });
  }

  ngOnInit(): void {
    this.getBlogPosts();
    // this.getBlogTags();
  }
}
