import { Directive, ElementRef, Renderer, Input } from '@angular/core';

@Directive({ selector: '[user]' })

export class UserDirective {
	constructor(el: ElementRef, renderer: Renderer) {

		if (localStorage.getItem("currentUser")){
			
			var localstore = JSON.parse(localStorage.getItem('currentUser'));
			el.nativeElement.innerHTML = localstore.username; 
			renderer.setElementStyle(el.nativeElement, 'display', 'true')
			
		}
	}
}