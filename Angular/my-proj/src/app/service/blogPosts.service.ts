import { Injectable }                               from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod }  from '@angular/http';
import { Observable }                               from 'rxjs/Rx';

import { BlogPost }                                 from '../model/blogPost';
import { Comment  }                                 from '../model/comment';
import { environment }                              from '../environment';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class BlogPostService {
  private url = "http://localhost:8000/api/";
  private blogPostsURL = this.url + 'billets/';
  private commentURL = this.url + 'commentaires/';
  private blogPostsByTagURL = this.url + 'tags/';
  private postLogin = this.url + 'login_check' ;
  
  private localstore = JSON.parse(localStorage.getItem('currentUser'));  
  private auth = "";
  
    constructor(private http: Http) { 
     
    }

    getBlogPosts(): Observable<BlogPost[]> {
     
      return this.http.get(this.blogPostsURL)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Servor error'));
    }

    getBlogPostOne(id : number): Observable<BlogPost> {

      return this.http.get(this.blogPostsURL + id)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Servor error'));
    }

    getBlogPostByTag(id : number): Observable<BlogPost[]> {
     
      return this.http.get(this.blogPostsByTagURL + id)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Servor error'));
    }

    createPost(title: string, content: string): Observable<any> {
     
      this.auth = this.localstore.token.token; 
      let headers = new Headers();
      headers.append('Content-Type','application/json');
      //headers.append('Authorization','Bearer ' + this.auth); 

      let dataToSend = JSON.stringify({ title: title, content: content, author: "adritaptap" });

      return this.http.post(this.blogPostsURL + '?bearer=' + this.auth, dataToSend, {headers : headers})
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Servor error'));
    }

    getBlogTags(): Observable<any[]> {
      return this.http.get(this.blogPostsByTagURL)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Servor error'));
    }

    getComment(): Observable<Comment[]> {
      return this.http.get(this.commentURL)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Servor error'));
    }

    createComment(author: string, content: string, idBillet: string): Observable<Comment> {

      let headers = new Headers();
      headers.append('Content-Type','application/json');
      let dataToSend = JSON.stringify({ author: author, content: content, idBillet: idBillet });
      return this.http.post(this.commentURL, dataToSend, {headers : headers})
      .map((res: Response) => res.json() as Comment)
      .catch((error: any) => Observable.throw(error.json().error || 'Servor error'));
    }

    updateCommenStatus(status : string, idComment: number): Observable<Comment> {

      let headers = new Headers();
      headers.append('Content-Type','application/json');
      let dataToSend = JSON.stringify({ status: status });
      console.log(dataToSend);
      return this.http.put(this.commentURL + idComment, dataToSend, {headers : headers})
      .map((res: Response) => res.json() as Comment)
      .catch((error: any) => Observable.throw(error.json().error || 'Servor error'));
    }

    sendLogin(username: string, password: string): Observable<any> {
      let headers = new Headers()
      headers.append('Content-Type','application/json');
      let dataToSend = JSON.stringify({ _username: username, _password: password});

      return this.http.post(this.postLogin, dataToSend, {headers : headers})
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Servor error'));
    }

    private handleError(error: any): Promise<any> {
      console.error('An error occurred', error);
      return Promise.reject(error.message || error);
    }
  }
