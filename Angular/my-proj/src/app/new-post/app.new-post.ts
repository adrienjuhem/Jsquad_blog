import { Component, OnInit } from '@angular/core';

import { BlogPostService } from '../service/blogPosts.service';
import { Location }          from '@angular/common';

@Component({
  selector      : '',
  templateUrl   : 'app/new-post/app.new-post.html',
  styleUrls     : ['app/new-post/app.new-post.css'],
  providers     : [BlogPostService]
})

export class AppNewPost {

  constructor(private blogPostService: BlogPostService,
              private location: Location)  { }

  title :   string;
  content:  string;
  BlogPost: any = {};

  save(): void {
    this.blogPostService.createPost(this.title, this.content).subscribe(() => this.goBack());
  }

  goBack(): void {
    this.location.back();
  }
}
