import { NgModule }             from '@angular/core';
import { BrowserModule }        from '@angular/platform-browser';
import { AppComponent }         from './app.component';
import { AppBoard }             from './board/app.board';
import { AppNewPost }           from "./new-post/app.new-post";
import { AppComment }           from "./comment/app.comment";
import { FormsModule }          from '@angular/forms';
import { AppBoardDetail }       from './board-detail/app.board-detail';
import { AppBoardTag }          from './board-tag/app.board-tag';

import { RouterModule }         from '@angular/router';
import { HttpModule }           from '@angular/http';

import { AppRoutingModule }     from './app-routing.module';
import { Login }                from "./login/app.login";
import { UserDirective } from './directive/app.directive';



@NgModule({
	imports:      [ BrowserModule, AppRoutingModule, FormsModule, HttpModule],
	declarations: [ AppComponent, AppBoard, AppNewPost, UserDirective, Login, AppComment, AppBoardDetail, AppBoardTag],
	bootstrap:    [ AppComponent ]
})

export class AppModule { }
