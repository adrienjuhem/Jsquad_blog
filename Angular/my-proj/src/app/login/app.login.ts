import { Component}           from '@angular/core';

import { BlogPostService } from '../service/blogPosts.service';
import { Location }          from '@angular/common';

@Component({
  selector      : '',
  templateUrl   : 'app/login/app.login.html',
  styleUrls     : [`app/login/app.login.css`],
  providers     : [BlogPostService]
})

export class Login {

  constructor(private blogPostService: BlogPostService,
              private location: Location)  { }

  private username :   string;
  private password:  string;
  private token: string;
  

  send(): void {
  	console.log('click : ' + this.username + ' | ' + this.password );
  	this.blogPostService.sendLogin(this.username, this.password).subscribe(token => {this.token = token, 
      console.log(this.token);
      localStorage.setItem('currentUser', JSON.stringify({ token: this.token, username: this.username }));
         this.location.go('accueil');
    });
    
  }
 
}
