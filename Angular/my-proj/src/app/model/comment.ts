export class Comment {
  id          : number;
  content     : string;
  created_at  : string;
  title       : string;
  id_billet   : number;
  status      : string;
}
