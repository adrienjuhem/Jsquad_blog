export class BlogPost {
  id      : number;
  date    : string;
  title   : string;
  billets : string;
  name    : string;
}
