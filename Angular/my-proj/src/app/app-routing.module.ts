import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent }         from './app.component';
import { AppBoard }             from './board/app.board';
import { AppNewPost }           from "./new-post/app.new-post";
import { Login }                from "./login/app.login";
import { AppBoardDetail }       from "./board-detail/app.board-detail";
import { AppBoardTag }          from './board-tag/app.board-tag';


const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'accueil',        component: AppBoard },
  { path: 'creation',       component: AppNewPost },
  { path: 'login',          component: Login },
  { path: 'billet/:id',     component: AppBoardDetail},
  { path: 'tag/:id',        component: AppBoardTag}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
