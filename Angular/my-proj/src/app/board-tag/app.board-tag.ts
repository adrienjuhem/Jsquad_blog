import { Component, OnInit }      from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { BlogPost }               from '../model/blogPost';
import { Comment }                from '../model/comment';
import { BlogPostService }        from '../service/blogPosts.service';
import { AppComment }             from '../comment/app.comment';


@Component({
  selector: 'truc',
  templateUrl: 'app/board-tag/app.board-tag.html',
  styleUrls: ['app/board-tag/app.board-tag.css'],
  providers: [BlogPostService]
})

export class AppBoardTag {
  private id: number;
  private sub: any;
  blogPosts: any[];
  blogTags: any[];
  name: string;

  constructor(
    private route: ActivatedRoute, 
    private blogPostService: BlogPostService
  ) { }

  private getBlogPostsByTag(id: number) {
    this.blogPostService.getBlogPostByTag(id).subscribe(blogPosts => {
      // this.blogPosts = blogPosts.billets;
      // this.name = blogPosts.name;
      console.log(this.blogPosts);
    });
  }

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
    });
    this.getBlogPostsByTag(this.id);

  }
}
